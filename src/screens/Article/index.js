import React, { useEffect, useState, useContext} from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import Moment from 'react-moment';
import { BookmarkButton, Loader } from '../../components';
import { BookmarkContext } from '../../context';
import './index.css';

function Article(){
  const { id } = useParams();
  const history = useHistory();
  const [image, setImage] = useState({})
  const [content, setContent] = useState({})
  const [isLoading, setIsLoading] = useState(true);
  const { bookmarks, setBookmarks } = useContext(BookmarkContext);
  
  const handleBookmarkEvent = () => {
    if(_.some(bookmarks, content)){
      _.remove(bookmarks, (bookmark) => {
        return bookmark.id === content.id
      });
      setBookmarks(bookmarks)
    }else{
      setBookmarks([...bookmarks, content])
    }
  }

  useEffect(() => {
    const path = _.replace(history.location.pathname, '/article', '');

    const fetchData = async () => {
      try{
        const data = await axios(`https://content.guardianapis.com/${path}`,{
          params: {
            'api-key': process.env.REACT_APP_API_KEY,
            'show-blocks': 'main',
            'show-fields': 'headline,body,trailText,thumbnail',
          }
        })
  
        //Extract data
        const imageData = _.filter(_.get(data, 'data.response.content.blocks.main.elements'), (each) => {
          return each.type === "image"
        });
  
        setImage(imageData)
        setContent(_.get(data, 'data.response.content'))
        setIsLoading(false)
      }catch(error){
        setIsLoading(false)
        history.push('/not-found')
      }
    }
    fetchData()
  },[history])

  if (isLoading) {
    return <div className="loading-container"><Loader /></div>;
  }

  return(
    <div className="article-screen">
      <div>
        <div className="article-bookmark">
          <BookmarkButton 
            keyName='view-bookmarks'
            name={_.some(bookmarks, content) ? 'REMOVE BOOKMARK':'ADD BOOKMARK'}
            markIconOn={_.some(bookmarks, content) ? false : true}
            clickFunc={() => handleBookmarkEvent()}
          />
        </div>
        <Moment className="publication-date" format="ddd D MMM YYYY HH.mm">
          {_.get(content,'webPublicationDate')}
        </Moment>
        <div className="article-body">
          <div className="article-headline highlight-text">
            {_.get(content,'fields.headline')}
          </div>
          <div
            className="article-sub-content highlight-text" dangerouslySetInnerHTML={{ __html: _.get(content,'fields.trailText') }} />
          <hr />
          <div >
            <div className="article-image-absolute" dangerouslySetInnerHTML={{ __html: _.get(content,'fields.body') }} >
            </div>
          </div>
        </div>
      </div>
      {
        id === "sport" ? (<div></div>) : 
        (
          <div className="article-image-container">
            {
              _.map(image,(each) => {
                const master = _.find(each.assets,(each) => {
                  return _.has(each, 'typeData.isMaster')
                })
                return (
              <div className="article-image">
                <img
                  src={_.get(master, 'file')}
                  alt={_.get(each, 'imageTypeData.alt')}
                />
                <p>{ _.get(each, 'imageTypeData.caption') }</p>
              </div>
              )})
            }
          </div>
        )
      }
    </div>
  )
}

export default Article