import React, { useContext, useState, useEffect  } from 'react';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import moment from 'moment';
import { DropDown, Card, Loader} from '../../components'
import { BookmarkContext } from '../../context';
import './index.css';

function ViewBookmark(){
  const history = useHistory();
  const [filter, setFilter] = useState('newest')
  const { bookmarks } = useContext(BookmarkContext);
  const [ myBookmaks, setMyBookmarks ] = useState([])
  const [isLoading, setIsLoading] = useState(true);

  const handleRoute = (data) => {
    const id = _.get(data, 'id')
    history.push(`/article/${id}`)
  }

  useEffect(() => {
    setIsLoading(true)
    const sortMethod = filter ==='newest'? 'desc' : 'asc'
    const sortBookmarks = _.orderBy(bookmarks, (o) => {
      return moment(o.webPublicationDate).format('YYYYMMDD')
    }, [sortMethod]);
    setMyBookmarks(sortBookmarks)
    setIsLoading(false)
  },[filter, bookmarks])

  return(
      <div className="view-bookmark-screen">
        {
          isLoading ? (<div className="loading-container"><Loader /></div>) : (<div></div>)
        }
        <div className="title-section">
          <h1>All bookmark</h1>
          <div className="tools">
            <div className="home-dropdown">
              <DropDown onChangeFunc={(e) => setFilter(e.target.value)}/>
            </div>
          </div>
        </div>
      <div className="view-bookmark-container">
      {
          _.map(myBookmaks, (value, index) => (
            <div key={`bookmark-news-${index}`} >
              <Card
                clickFunc={() => handleRoute(value)}
                title={_.get(value, 'webTitle', '')}
                description={_.get(value, 'fields.trailText', '')}
                image={_.get(value, 'fields.thumbnail', '')}
              />
            </div>
          ))
        }
      </div>
  
    </div>
  )
}

export default ViewBookmark
