import React from 'react';
import './index.css';

function NotFound(){
  return(
    <div className="not-found-screen">
      Not Found
    </div>
  )
}

export default NotFound