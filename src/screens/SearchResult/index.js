import React, { useEffect, useState, useRef  } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import { DropDown, BookmarkButton, Loader, Card} from '../../components'
import './index.css';

function SearchResult(){
  const history = useHistory();
  const moreLoader = useRef(null);
  const { query } = useParams()
  const [page, setPage] = useState(1);
  const [filter, setFilter] = useState('newest')
  const [searchResult, setSearchResult] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const handleObserver = (entities) => {
    const target = entities[0];
    if (target.isIntersecting) {   
        setPage((page) => page + 1)
    }
  }
  
  const handleRoute = (data) => {
    const id = _.get(data, 'id')
    history.push(`/article/${id}`)
  }
  
  useEffect(() => {
    setSearchResult([])
  },[query, filter])

  useEffect(() => {
    const fetchData = async () => {
      try{
        const data = await axios(`https://content.guardianapis.com/search`,{
          params: {
            'api-key': process.env.REACT_APP_API_KEY,
            'show-fields': 'headline,thumbnail',
            'order-by': filter,
            'type': 'article',
            'page-size': 15,
            'page': page,
            'q': query,
            'query-fields': 'headline',
          }
        })
  
        const response = _.get(data, 'data.response.results')
        setSearchResult([...searchResult, ...response])
      }catch(error){
        console.log(error)
      }
    }
    fetchData()
    setIsLoading(false)
  },[query, filter, page])

  useEffect(() => {
    var options = {
       root: null,
       rootMargin: "20px",
       threshold: 1.0
    };
    const observer = new IntersectionObserver(handleObserver, options);
    if (moreLoader.current) {
       observer.observe(moreLoader.current)
    }
}, []);

  return(
      <div className="search-result-screen">
        {
          isLoading ? (<div className="loading-container"><Loader /></div>) : (<div></div>)
        }
        <div className="title-section">
          <h1>Search result</h1>
          <div className="tools">
            <BookmarkButton 
              keyName='view-bookmarks'
              name='VIEW BOOKMARK'
              clickFunc={() => history.push(`/view-bookmarks`)}
            />
            <div className="home-dropdown">
              <DropDown onChangeFunc={(e) => setFilter(e.target.value)}/>
            </div>
          </div>
        </div>
      <div className="result-container">
        {
          _.map(searchResult, (value, index) => (
            <div key={`result-news-${index}`} >
              <Card
                clickFunc={() => handleRoute(value)}
                title={_.get(value, 'webTitle', '')}
                description={_.get(value, 'fields.trailText', '')}
                image={_.get(value, 'fields.thumbnail', '')}
              />
            </div>
          ))
        }
      </div>
      <div className="loading" ref={moreLoader}>
          {/* <h2>Load More</h2> */}
      </div>
    </div>
  )
}

export default SearchResult
