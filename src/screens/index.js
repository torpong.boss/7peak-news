export { default as Home } from './Home';
export { default as Article } from './Article';
export { default as NotFound } from './NotFound';
export { default as SearchResult } from './SearchResult';
export { default as ViewBookmark } from './ViewBookmark';