import React from 'react';
import _ from 'lodash';
import { Card } from '../../../components/'
import './index.css';

function AdditionalStorySection({stories, clickFunc}){
  return(
    <div className="additional-story-section">
      {
        _.map(stories, (value, index) => (
          <div key={`additional-news-${index}`} >
            <Card
              clickFunc={() => clickFunc(value)}
              title={_.get(value, 'webTitle', '')}
              description={_.get(value, 'fields.trailText', '')}
              image={_.get(value, 'fields.thumbnail', '')}
            />
          </div>
        ))
      }
    </div>
  )
}

export default AdditionalStorySection