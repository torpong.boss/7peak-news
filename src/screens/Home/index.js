import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import { DropDown, BookmarkButton, Loader} from '../../components'
import TopStorySection from './TopStorySection'
import AdditionalStorySection from './AdditionalStorySection'
import './index.css';

function Home(){
  const history = useHistory();
  const [news, setNews] = useState({});
  const [sports, setSports] = useState({});
  const [filter, setFilter] = useState('newest')
  const [isLoading, setIsLoading] = useState(true);

  const handleRoute = (data) => {
    const id = _.get(data, 'id')
    history.push(`/article/${id}`)
  }

  useEffect(() => {
    const newsData = axios.get('https://content.guardianapis.com/search?',{
      params: {
        'api-key': process.env.REACT_APP_API_KEY,
        section: 'news',
        'page-size': 8,
        'order-by': filter,
        'show-fields': 'trailText,thumbnail',
      }
    })
    const sportData = axios.get('https://content.guardianapis.com/search?',{
      params: {
        'api-key': process.env.REACT_APP_API_KEY,
        section: 'sport',
        'page-size': 3,
        'order-by': filter,
        'show-fields': 'trailText,thumbnail',
        'type': 'article'
      }
    })

    Promise.all([newsData, sportData]).then((values) => {
      setNews(_.get(values[0], 'data.response.results', {}))
      setSports(_.get(values[1], 'data.response.results', {}))
    })
    .then(function () {
      setIsLoading(false)
    });
  },[filter])

  if (isLoading) {
    return <div className="loading-container"><Loader /></div>;
  }

  return(
    <div className="home-screen">
      <div className="title-section">
        <h1>Top stories</h1>
        <div className="tools">
          <BookmarkButton 
            keyName='view-bookmarks'
            name='VIEW BOOKMARK'
            clickFunc={() => history.push(`/view-bookmarks`)}
          />
          <div className="home-dropdown">
            <DropDown onChangeFunc={(e) => setFilter(e.target.value)}/>
          </div>
        </div>
      </div>
      <TopStorySection
        stories={news}
        clickFunc={handleRoute}
      />
      <AdditionalStorySection
        stories={_.slice(news,5,8)}
        clickFunc={handleRoute}
      />
      <div className="title-section">
        <span>Sports</span>
      </div>
      <AdditionalStorySection
        stories={sports}
        clickFunc={handleRoute}
      />
    </div>
  )
}

export default Home