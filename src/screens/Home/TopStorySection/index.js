import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import { Card, CardTextOverlay, CardNoImage } from '../../../components'
import './index.css';

function TopStorySection({stories, clickFunc}){
  const [data, setData] = useState(stories)

  useEffect(() => {
    const colorArray = ['#388E3C','#D32F2F','#FFC107','#2196F3','#388E3C']

    const editedData = _.reduce(stories, (result, each, index) => {
      result.push(_.assign(each,{color: colorArray[index]}))
      return result
    },[])

    setData(editedData)
  },[stories])

  return(
    <div className="top-story-section">
      <div className="section-split">
        <Card
          key={`top-stories-0`}
         title={_.get(data[0], 'webTitle', '')}
         description={_.get(data[0], 'fields.trailText', '')}
         image={_.get(data[0], 'fields.thumbnail', '')}
         border={_.get(data[0], 'color', '')}
         clickFunc={() => clickFunc(data[0])}
        />
      </div>
      <div className="section-split">
        <div className="upper-row-story">
          {
            _.map(_.slice(data,1,3), (each,index) => (
              <div
                key={`top-stories-card-overlay-${index}`}
                onClick={() => clickFunc(each)}
              >
                <CardTextOverlay
                  title={_.get(each, 'webTitle', '')}
                  image={_.get(each, 'fields.thumbnail', '')}
                  border={_.get(each, 'color', '')}
                />
              </div>
            ))
          }
        </div>
        <div className="lower-row-story">
        {
          _.map(_.slice(data,3,5), (each, index) => (
            <div
              key={`top-stories-no-image-${index}`}
              onClick={() => clickFunc(each)}
            >
              <CardNoImage
                title={_.get(each, 'webTitle', '')}
                border={_.get(each, 'color', '')}
              />
            </div>
          ))
        }
        </div>
      </div>
    </div>
  )
}

export default TopStorySection