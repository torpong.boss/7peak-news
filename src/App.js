import './App.css';
import Layout from './layouts'
import {Home, Article, NotFound, SearchResult, ViewBookmark} from './screens'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { BookmarkProvider } from './context';
import React from 'react';
import Utils from './utils';

function App() {
  const keepedBookmark = Utils.localStorage.getItem('bookmarks');
  return (
    <Router>
      <BookmarkProvider value={keepedBookmark}>
        <div className="App">
              <Layout>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route path="/article/:id" component={Article} />
                  <Route path="/search/:query" component={SearchResult} />
                  <Route path="/view-bookmarks" component={ViewBookmark} />
                  <Route component={NotFound} />
                </Switch>
              </Layout>
        </div>
      </BookmarkProvider>
    </Router>


  );
}

export default App;

