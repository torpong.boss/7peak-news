import React, { /* useState */ } from 'react';
import Logo_White from '../assets/images/Logo_White.png'
import './index.css';
import SearchBox from '../../components/SearchBox'
import { useHistory } from 'react-router-dom';

function Header({ header, content }) {
  const history = useHistory();

  return (
    <div className="seven-peaks-header">
      <div className="header-content">
        <div className="seven-peaks-icons" onClick={() => history.push('/')}>
          <img
            src={Logo_White}
            alt="seven-peaks-logo"
          />
        </div>
        <SearchBox></SearchBox>
      </div>
    </div>
  );
}



export default Header;
