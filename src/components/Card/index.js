
import React from 'react';
import './index.css';

function Card({image, description, title, border, clickFunc}){

  const imageStyle = (image) => ({
    backgroundImage: (image) ? `url("${image}")` : `2`,
  });

  return(
    <div 
      onClick={() => clickFunc()}
      className={`card-container ${image ? '' : 'no-image'}`} 
      style={imageStyle(image)}>
      <div className="text-overlay" style={{'borderColor': border ? border : 'red'}}>
        <p className="headline">{title}</p>
        <p className="description"
          dangerouslySetInnerHTML={{ __html: description }} 
        />
      </div>
    </div>
    )
}

export default Card