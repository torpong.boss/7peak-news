
import React from 'react';
import './index.css';
import _ from 'lodash';

function DropDown({onChangeFunc}){

  const defaultOption = [
    {
      display:'Newest First',
      value : 'newest',
    },
    {
      display:'Oldest First',
      value: 'oldest',
    }
  ]

  return(
    <select key='drop-down' className="dropdown" name="select" onChange={onChangeFunc}>
      {
        _.map(defaultOption, (each) => {
          return (
            <option
              value={_.get(each,'value')}
              key={_.get(each,'value')}
            >
              {_.get(each, 'display')}
            </option>);
        })
      }
    </select>
    )
}

export default DropDown