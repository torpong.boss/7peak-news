import React, { useState, useRef, useEffect } from 'react';
import { ReactComponent as SearchIcon } from '../assets/images/search-icon@2x.svg'
import './index.css';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';

function SearchBox(){
  const [isExpand, setExpand] = useState(false)
  const searchBarRef = useRef(null);
  const inputRef = useRef();
  const history = useHistory();

  const handleClickOutside = (event) => {
    if (searchBarRef.current && !searchBarRef.current.contains(event.target)) {
        if (inputRef.current.value === ""){
          setExpand(false)
        }
    }
  }
  
  const handleSearch = _.debounce((event) => {
    if (event.target.value !== "") {
      history.push(`/search/${event.target.value}`)
    }
  },1000)

  useEffect(() => {
      document.addEventListener("click", handleClickOutside, true);
      return () => {
          document.removeEventListener("click", handleClickOutside, true);
      };
  }, [searchBarRef]);


  return (
    <div className={`search-bar ${isExpand ? "expand" : ""}`} ref={searchBarRef}>
      <input ref={inputRef} className="search-bar-input" placeholder="Search all news" type="search" name="search" id="search" 
        onChange={(e) => handleSearch(e)}
      />
      <div className="substitute"></div>
      <div className="search-bar-icon">
        <input 
          className="search-bar-submit"
          type="submit"
          value=""
          onClick={() => setExpand(!isExpand)}
        />
        <SearchIcon></SearchIcon>
      </div>
    </div>
  );
}





export default SearchBox;
