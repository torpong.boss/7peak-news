import React from 'react';
import './index.css';

function BookmarkButton({keyName, name, markIconOn=true, clickFunc}){

  return (
    <button 
      key={`bookmark-${keyName}`}
      className={`bookmark-button ${markIconOn ? 'icon-bookmark-on' : 'icon-bookmark-off'}`}
      onClick={() => clickFunc()}
    >
      {name}
    </button>
  );
}

export default BookmarkButton;