import React from 'react';
// import PropTypes from 'prop-types';
import './index.css';

function Loader() {
  return (
    <div className="cover-screen">
      <div className="spinner"></div>
    </div>
  );
}

Loader.propTypes = {
};

export default Loader;
