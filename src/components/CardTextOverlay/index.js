
import React from 'react';
import './index.css';

function CardTextOverlay({image, title, border}){
  return(
    <div className={`card-container ${image ? '' : 'no-image'}`} style={{'backgroundImage': image ? `url("${image}")` : '-'}}>
      <div className="text-only-overlay"
        style={{'borderColor': border ? border : 'red'}}>
        <p className="headline">{title}</p>
      </div>
    </div>
    )
}

export default CardTextOverlay