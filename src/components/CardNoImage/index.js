
import React from 'react';
import './index.css';

function CardNoImage({title, border}){
  return(
    <div className="card-container">
      <div className="card-no-image" style={{'borderColor': border ? border : 'red'}}>
        <p className="headline">{title}</p>
      </div>
    </div>
    )
}

export default CardNoImage