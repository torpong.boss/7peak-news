import React, { useState, createContext } from 'react';
import Utils from '../utils';

export const BookmarkContext = createContext();

export const BookmarkProvider = ({ children, value }) => {
  const [context, setContext] = useState(value || []);

  const handler = (bookmark) => {
    setContext([...bookmark]);
    Utils.localStorage.setItem('bookmarks', [...bookmark]);
  };

  return (
    <BookmarkContext.Provider value={{ bookmarks: context, setBookmarks: handler }}>
      {children}
    </BookmarkContext.Provider>
  );
};
