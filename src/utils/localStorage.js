const setItem = (key, item) => {
  localStorage.setItem(key, JSON.stringify(item));
  return true;
};

const getItem = (key) => JSON.parse(localStorage.getItem(key));

const exportedObject = {
  setItem,
  getItem,
};

export default exportedObject;
